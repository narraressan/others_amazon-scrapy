
# -*- coding: utf-8 -*-
from myamazonstore import AmazonStoreItem
from scrapy.selector import Selector
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import requests, random, json, csv, datetime


class AmazonComCrawler():

	def __init__(self, urls = []):
		file_name = open(str(random.random()) + "_" + datetime.datetime.today().strftime('%Y-%m-%d') + '.csv', 'w')
		fieldnames = ["category","asin","store_name","add_on","product_url","title","rating","reviews","price","other_shipping","quantity","shipper","shipping_time","features","brand","brand_url","UPC","Manufacturer","ItemModelNo","AvailableDate","Color","Size","Style","StyleName","Rank1","Rank2","Rank3","Rank4","image_1","image_2","image_3","image_4","image_5","image_6","image_7"]
		writer = csv.DictWriter(file_name, fieldnames=fieldnames)
		writer.writeheader()

		try:
			# PROXY = "38.96.9.236:8008"

			chrome_options = Options()
			# chrome_options.add_argument("--headless")
			chrome_options.add_argument("--incognito")
			chrome_options.add_argument("--disable-gpu")
			chrome_options.add_argument("--disable-web-security")
			# chrome_options.add_argument("--proxy-server=%s" % PROXY)
			chrome_options.add_experimental_option("prefs", { "profile.managed_default_content_settings.images": 2 })

			self.browser = webdriver.Chrome(executable_path=r"chromedriver.exe", chrome_options=chrome_options)
		except Exception as err:
			# print(err)
			pass


		prod_links = []
		for url in urls:
			html = self.custom_download(url)
			if html != None:
				prod_links.extend(self.parse(html))


		for link in prod_links:
			for page in link["urls"]:
				try:
					# print(page + " ---- " + link["category"])
					html = self.custom_download(page)
					rowData = self.each_detail(html, link["category"], page)
					writer.writerow(rowData) 
				except Exception as err:
					# print(err)
					pass

		self.browser.quit()



	def custom_download(self, url):
		retry = 1
		try:
			self.browser.get(url)
			self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			raw = self.browser.page_source.encode("utf8","ignore")
			# self.browser.quit()

			# print("Downloaded: " + str(url))
			return raw

		except Exception as err:
			print("ERR: " + str(err) + " -- retry: " + str(retry) + " in 5 seconds")
			
			# if browser:
			# 	browser.quit()

			retry = retry + 1
			if retry > 3: # error retry upto 3 times
				return None
			else:
				# retry
				self.custom_download(url)


	def parse(self, response):
		hxs=Selector(text=response)
		prod_link=hxs.xpath('//a[contains(@class,"a-link-normal s-access-detail-page")]/@href').extract()
		category=hxs.xpath('//meta[@name="keywords"]/@content').extract()

		next_page=hxs.xpath('//*[@class="pagnNext"][1]/@href').extract()
		collections = [{ "urls": prod_link, "category": category[0] }]
		try:
			if len(next_page)>0:
				html = self.custom_download("http://www.amazon.com"+next_page[0])
				collections.extend(self.parse(html))
		except Exception as err:
			# print(err)
			pass

		return collections


	def each_detail(self, response, category, page_url):
		# REFERENCE: https://doc.scrapy.org/en/latest/topics/selectors.html
		sel=Selector(text=response)
		text_1=sel.xpath('//*[@class="label"][1]//text()').extract()
		text_2=sel.xpath('//*[@class="label"][2]//text()').extract()
		text_3=sel.xpath('//*[@class="label"][3]//text()').extract()
		text_4=sel.xpath('//*[@class="label"][4]//text()').extract()
		text_5=sel.xpath('//*[@class="label"][5]//text()').extract()
		text_6=sel.xpath('//*[@class="label"][6]//text()').extract()
		text_7=sel.xpath('//*[@class="label"][7]//text()').extract()
		text_8=sel.xpath('//*[@class="label"][8]//text()').extract()
		text_9=sel.xpath('//*[@class="label"][9]//text()').extract()
		text_10=sel.xpath('//*[@class="label"][10]//text()').extract()
		text_11=sel.xpath('//*[@class="label"][11]//text()').extract()
		text_12=sel.xpath('//*[@class="label"][12]//text()').extract()
		text_13=sel.xpath('//*[@class="label"][13]//text()').extract()
		text_14=sel.xpath('//*[@class="label"][14]//text()').extract()
		text_15=sel.xpath('//*[@class="label"][15]//text()').extract()
		text_16=sel.xpath('//*[@class="label"][16]//text()').extract()
		text_17=sel.xpath('//*[@class="label"][17]//text()').extract()
		text_18=sel.xpath('//*[@class="label"][18]//text()').extract()
		text_19=sel.xpath('//*[@class="label"][19]//text()').extract()
		text_20=sel.xpath('//*[@class="label"][20]//text()').extract()
		
		text_21=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/th[1]/text()').extract()
		text_22=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/th[1]/text()').extract()
		text_23=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/th[1]/text()').extract()
		text_24=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/th[1]/text()').extract()
		text_25=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/th[1]/text()').extract()
		text_26=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/th[1]/text()').extract()
		text_27=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/th[1]/text()').extract()
		text_28=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/th[1]/text()').extract()
		text_29=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/th[1]/text()').extract()
		text_30=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/th[1]/text()').extract()
		text_31=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/th[1]/text()').extract()
		text_32=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/th[1]/text()').extract()
		text_33=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/th[1]/text()').extract()
		text_34=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/th[1]/text()').extract()
		text_35=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/th[1]/text()').extract()
		text_36=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/th[1]/text()').extract()
		text_37=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/th[1]/text()').extract()
		text_38=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/th[1]/text()').extract()
		text_39=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/th[1]/text()').extract()
		text_40=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/th[1]/text()').extract()
		
		text_41=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/b/text()').extract()
		text_42=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/b/text()').extract()
		text_43=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/b/text()').extract()
		text_44=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/b/text()').extract()
		text_45=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/b/text()').extract()
		text_46=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/b/text()').extract()
		text_47=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/b/text()').extract()
		text_48=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/b/text()').extract()
		text_49=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/b/text()').extract()
		text_50=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/b/text()').extract()
		text_51=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/b/text()').extract()
		text_52=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/b/text()').extract()
		text_53=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/b/text()').extract()
		text_54=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/b/text()').extract()
		text_55=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/b/text()').extract()
		text_56=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/b/text()').extract()
		text_57=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/b/text()').extract()
		text_58=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/b/text()').extract()
		text_59=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/b/text()').extract()
		text_60=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/b/text()').extract()
		
		text_61=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[1]/th[1]/text()').extract()
		text_62=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[2]/th[1]/text()').extract()
		text_63=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[3]/th[1]/text()').extract()
		text_64=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[4]/th[1]/text()').extract()
		text_65=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[5]/th[1]/text()').extract()
		text_66=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[6]/th[1]/text()').extract()
		text_67=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[7]/th[1]/text()').extract()
		text_68=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[8]/th[1]/text()').extract()
		text_69=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[9]/th[1]/text()').extract()
		text_70=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[10]/th[1]/text()').extract()
		text_71=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[11]/th[1]/text()').extract()
		text_72=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[12]/th[1]/text()').extract()
		text_73=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[13]/th[1]/text()').extract()
		text_74=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[14]/th[1]/text()').extract()
		text_75=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[15]/th[1]/text()').extract()
		text_76=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[16]/th[1]/text()').extract()
		text_77=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[17]/th[1]/text()').extract()
		text_78=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[18]/th[1]/text()').extract()
		text_79=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[19]/th[1]/text()').extract()
		text_80=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[20]/th[1]/text()').extract()
		
		
		asin=""
	  
		if "ASIN" in "".join(text_1).strip().strip():
			asin=sel.xpath('//*[@class="value"][1]//text()').extract()
		elif "ASIN" in "".join(text_2).strip().strip():
			asin=sel.xpath('//*[@class="value"][2]//text()').extract()
		elif "ASIN" in "".join(text_3).strip().strip():
			asin=sel.xpath('//*[@class="value"][3]//text()').extract()
		elif "ASIN" in "".join(text_4).strip().strip():
			asin=sel.xpath('//*[@class="value"][4]//text()').extract()
		elif "ASIN" in "".join(text_5).strip().strip():
			asin=sel.xpath('//*[@class="value"][5]//text()').extract()
		elif "ASIN" in "".join(text_6).strip().strip():
			asin=sel.xpath('//*[@class="value"][6]//text()').extract()
		elif "ASIN" in "".join(text_7).strip().strip():
			asin=sel.xpath('//*[@class="value"][7]//text()').extract()
		elif "ASIN" in "".join(text_8).strip().strip():
			asin=sel.xpath('//*[@class="value"][8]//text()').extract()
		elif "ASIN" in "".join(text_9).strip().strip():
			asin=sel.xpath('//*[@class="value"][9]//text()').extract()
		elif "ASIN" in "".join(text_10).strip().strip():
			asin=sel.xpath('//*[@class="value"][10]//text()').extract()
		elif "ASIN" in "".join(text_11).strip().strip():
			asin=sel.xpath('//*[@class="value"][11]//text()').extract()
		elif "ASIN" in "".join(text_12).strip().strip():
			asin=sel.xpath('//*[@class="value"][12]//text()').extract()
		elif "ASIN" in "".join(text_13).strip().strip():
			asin=sel.xpath('//*[@class="value"][13]//text()').extract()
		elif "ASIN" in "".join(text_14).strip().strip():
			asin=sel.xpath('//*[@class="value"][14]//text()').extract()
		elif "ASIN" in "".join(text_15).strip().strip():
			asin=sel.xpath('//*[@class="value"][15]//text()').extract()
		elif "ASIN" in "".join(text_16).strip().strip():
			asin=sel.xpath('//*[@class="value"][16]//text()').extract()
		elif "ASIN" in "".join(text_17).strip().strip():
			asin=sel.xpath('//*[@class="value"][17]//text()').extract()
		elif "ASIN" in "".join(text_18).strip().strip():
			asin=sel.xpath('//*[@class="value"][18]//text()').extract()
		elif "ASIN" in "".join(text_19).strip().strip():
			asin=sel.xpath('//*[@class="value"][19]//text()').extract()
		elif "ASIN" in "".join(text_20).strip().strip():
			asin=sel.xpath('//*[@class="value"][20]//text()').extract()
		
		elif "ASIN" in "".join(text_21).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_22).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_23).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_24).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_25).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_26).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_27).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_28).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_29).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_30).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_31).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_32).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_33).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_34).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_35).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_36).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_37).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_38).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_39).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_40).strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/td[1]/text()').extract()
		

		elif "ASIN" in "".join(text_41).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/text()').extract()
		elif "ASIN" in "".join(text_42).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/text()').extract()
		elif "ASIN" in "".join(text_43).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/text()').extract()
		elif "ASIN" in "".join(text_44).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/text()').extract()
		elif "ASIN" in "".join(text_45).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/text()').extract()
		elif "ASIN" in "".join(text_46).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/text()').extract()
		elif "ASIN" in "".join(text_47).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/text()').extract()
		elif "ASIN" in "".join(text_48).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/text()').extract()
		elif "ASIN" in "".join(text_49).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/text()').extract()
		elif "ASIN" in "".join(text_50).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/text()').extract()
		elif "ASIN" in "".join(text_51).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/text()').extract()
		elif "ASIN" in "".join(text_52).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/text()').extract()
		elif "ASIN" in "".join(text_53).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/text()').extract()
		elif "ASIN" in "".join(text_54).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/text()').extract()
		elif "ASIN" in "".join(text_55).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/text()').extract()
		elif "ASIN" in "".join(text_56).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/text()').extract()
		elif "ASIN" in "".join(text_57).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/text()').extract()
		elif "ASIN" in "".join(text_58).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/text()').extract()
		elif "ASIN" in "".join(text_59).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/text()').extract()
		elif "ASIN" in "".join(text_60).strip():
			asin=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/text()').extract()
		
		elif "ASIN" in "".join(text_61).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_62).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[2]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_63).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[3]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_64).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[4]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_65).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[5]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_66).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[6]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_67).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[7]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_68).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[8]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_69).strip().strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[9]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_70).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[10]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_71).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[11]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_72).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[12]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_73).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[13]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_74).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[14]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_75).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[15]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_76).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[16]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_77).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[17]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_78).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[18]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_79).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[19]/td[1]/text()').extract()
		elif "ASIN" in "".join(text_80).strip():
			asin=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[20]/td[1]/text()').extract()
		

		upc=""
	  
		if "UPC:" in "".join(text_1).strip():
			upc=sel.xpath('//*[@class="value"][1]//text()').extract()
		elif "UPC:" in "".join(text_2).strip():
			upc=sel.xpath('//*[@class="value"][2]//text()').extract()
		elif "UPC:" in "".join(text_3).strip():
			upc=sel.xpath('//*[@class="value"][3]//text()').extract()
		elif "UPC:" in "".join(text_4).strip():
			upc=sel.xpath('//*[@class="value"][4]//text()').extract()
		elif "UPC:" in "".join(text_5).strip():
			upc=sel.xpath('//*[@class="value"][5]//text()').extract()
		elif "UPC:" in "".join(text_6).strip():
			upc=sel.xpath('//*[@class="value"][6]//text()').extract()
		elif "UPC:" in "".join(text_7).strip():
			upc=sel.xpath('//*[@class="value"][7]//text()').extract()
		elif "UPC:" in "".join(text_8).strip():
			upc=sel.xpath('//*[@class="value"][8]//text()').extract()
		elif "UPC:" in "".join(text_9).strip():
			upc=sel.xpath('//*[@class="value"][9]//text()').extract()
		elif "UPC:" in "".join(text_10).strip():
			upc=sel.xpath('//*[@class="value"][10]//text()').extract()
		elif "UPC:" in "".join(text_11).strip():
			upc=sel.xpath('//*[@class="value"][11]//text()').extract()
		elif "UPC:" in "".join(text_12).strip():
			upc=sel.xpath('//*[@class="value"][12]//text()').extract()
		elif "UPC:" in "".join(text_13).strip():
			upc=sel.xpath('//*[@class="value"][13]//text()').extract()
		elif "UPC:" in "".join(text_14).strip():
			upc=sel.xpath('//*[@class="value"][14]//text()').extract()
		elif "UPC:" in "".join(text_15).strip():
			upc=sel.xpath('//*[@class="value"][15]//text()').extract()
		elif "UPC:" in "".join(text_16).strip():
			upc=sel.xpath('//*[@class="value"][16]//text()').extract()
		elif "UPC:" in "".join(text_17).strip():
			upc=sel.xpath('//*[@class="value"][17]//text()').extract()
		elif "UPC:" in "".join(text_18).strip():
			upc=sel.xpath('//*[@class="value"][18]//text()').extract()
		elif "UPC:" in "".join(text_19).strip():
			upc=sel.xpath('//*[@class="value"][19]//text()').extract()
		elif "UPC:" in "".join(text_20).strip():
			upc=sel.xpath('//*[@class="value"][20]//text()').extract()
		
		elif "UPC:" in "".join(text_21).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_22).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_23).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_24).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_25).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_26).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_27).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_28).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_29).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_30).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_31).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_32).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_33).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_34).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_35).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_36).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_37).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_38).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_39).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_40).strip():
			upc=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/td[1]/text()').extract()
		

		elif "UPC:" in "".join(text_41).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/text()').extract()
		elif "UPC:" in "".join(text_42).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/text()').extract()
		elif "UPC:" in "".join(text_43).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/text()').extract()
		elif "UPC:" in "".join(text_44).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/text()').extract()
		elif "UPC:" in "".join(text_45).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/text()').extract()
		elif "UPC:" in "".join(text_46).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/text()').extract()
		elif "UPC:" in "".join(text_47).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/text()').extract()
		elif "UPC:" in "".join(text_48).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/text()').extract()
		elif "UPC:" in "".join(text_49).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/text()').extract()
		elif "UPC:" in "".join(text_50).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/text()').extract()
		elif "UPC:" in "".join(text_51).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/text()').extract()
		elif "UPC:" in "".join(text_52).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/text()').extract()
		elif "UPC:" in "".join(text_53).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/text()').extract()
		elif "UPC:" in "".join(text_54).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/text()').extract()
		elif "UPC:" in "".join(text_55).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/text()').extract()
		elif "UPC:" in "".join(text_56).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/text()').extract()
		elif "UPC:" in "".join(text_57).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/text()').extract()
		elif "UPC:" in "".join(text_58).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/text()').extract()
		elif "UPC:" in "".join(text_59).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/text()').extract()
		elif "UPC:" in "".join(text_60).strip():
			upc=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/text()').extract()
		
		elif "UPC:" in "".join(text_61).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[1]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_62).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[2]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_63).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[3]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_64).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[4]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_65).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[5]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_66).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[6]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_67).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[7]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_68).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[8]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_69).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[9]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_70).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[10]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_71).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[11]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_72).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[12]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_73).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[13]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_74).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[14]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_75).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[15]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_76).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[16]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_77).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[17]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_78).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[18]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_79).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[19]/td[1]/text()').extract()
		elif "UPC:" in "".join(text_80).strip():
			upc=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[20]/td[1]/text()').extract()
		
		#######################################################################################

		manufacturer=""
	  
		if "Manufacturer" in "".join(text_1).strip():
			manufacturer=sel.xpath('//*[@class="value"][1]//text()').extract()
		elif "Manufacturer" in "".join(text_2).strip():
			manufacturer=sel.xpath('//*[@class="value"][2]//text()').extract()
		elif "Manufacturer" in "".join(text_3).strip():
			manufacturer=sel.xpath('//*[@class="value"][3]//text()').extract()
		elif "Manufacturer" in "".join(text_4).strip():
			manufacturer=sel.xpath('//*[@class="value"][4]//text()').extract()
		elif "Manufacturer" in "".join(text_5).strip():
			manufacturer=sel.xpath('//*[@class="value"][5]//text()').extract()
		elif "Manufacturer" in "".join(text_6).strip():
			manufacturer=sel.xpath('//*[@class="value"][6]//text()').extract()
		elif "Manufacturer" in "".join(text_7).strip():
			manufacturer=sel.xpath('//*[@class="value"][7]//text()').extract()
		elif "Manufacturer" in "".join(text_8).strip():
			manufacturer=sel.xpath('//*[@class="value"][8]//text()').extract()
		elif "Manufacturer" in "".join(text_9).strip():
			manufacturer=sel.xpath('//*[@class="value"][9]//text()').extract()
		elif "Manufacturer" in "".join(text_10).strip():
			manufacturer=sel.xpath('//*[@class="value"][10]//text()').extract()
		elif "Manufacturer" in "".join(text_11).strip():
			manufacturer=sel.xpath('//*[@class="value"][11]//text()').extract()
		elif "Manufacturer" in "".join(text_12).strip():
			manufacturer=sel.xpath('//*[@class="value"][12]//text()').extract()
		elif "Manufacturer" in "".join(text_13).strip():
			manufacturer=sel.xpath('//*[@class="value"][13]//text()').extract()
		elif "Manufacturer" in "".join(text_14).strip():
			manufacturer=sel.xpath('//*[@class="value"][14]//text()').extract()
		elif "Manufacturer" in "".join(text_15).strip():
			manufacturer=sel.xpath('//*[@class="value"][15]//text()').extract()
		elif "Manufacturer" in "".join(text_16).strip():
			manufacturer=sel.xpath('//*[@class="value"][16]//text()').extract()
		elif "Manufacturer" in "".join(text_17).strip():
			manufacturer=sel.xpath('//*[@class="value"][17]//text()').extract()
		elif "Manufacturer" in "".join(text_18).strip():
			manufacturer=sel.xpath('//*[@class="value"][18]//text()').extract()
		elif "Manufacturer" in "".join(text_19).strip():
			manufacturer=sel.xpath('//*[@class="value"][19]//text()').extract()
		elif "Manufacturer" in "".join(text_20).strip():
			manufacturer=sel.xpath('//*[@class="value"][20]//text()').extract()
		
		elif "Manufacturer" in "".join(text_21).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_22).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_23).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_24).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_25).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_26).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_27).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_28).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_29).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_30).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_31).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_32).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_33).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_34).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_35).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_36).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_37).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_38).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_39).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_40).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/td[1]/text()').extract()
		

		elif "Manufacturer" in "".join(text_41).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/text()').extract()
		elif "Manufacturer" in "".join(text_42).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/text()').extract()
		elif "Manufacturer" in "".join(text_43).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/text()').extract()
		elif "Manufacturer" in "".join(text_44).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/text()').extract()
		elif "Manufacturer" in "".join(text_45).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/text()').extract()
		elif "Manufacturer" in "".join(text_46).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/text()').extract()
		elif "Manufacturer" in "".join(text_47).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/text()').extract()
		elif "Manufacturer" in "".join(text_48).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/text()').extract()
		elif "Manufacturer" in "".join(text_49).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/text()').extract()
		elif "Manufacturer" in "".join(text_50).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/text()').extract()
		elif "Manufacturer" in "".join(text_51).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/text()').extract()
		elif "Manufacturer" in "".join(text_52).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/text()').extract()
		elif "Manufacturer" in "".join(text_53).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/text()').extract()
		elif "Manufacturer" in "".join(text_54).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/text()').extract()
		elif "Manufacturer" in "".join(text_55).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/text()').extract()
		elif "Manufacturer" in "".join(text_56).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/text()').extract()
		elif "Manufacturer" in "".join(text_57).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/text()').extract()
		elif "Manufacturer" in "".join(text_58).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/text()').extract()
		elif "Manufacturer" in "".join(text_59).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/text()').extract()
		elif "Manufacturer" in "".join(text_60).strip():
			manufacturer=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/text()').extract()
		

		elif "Manufacturer" in "".join(text_61).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[1]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_62).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[2]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_63).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[3]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_64).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[4]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_65).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[5]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_66).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[6]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_67).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[7]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_68).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[8]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_69).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[9]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_70).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[10]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_71).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[11]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_72).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[12]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_73).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[13]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_74).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[14]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_75).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[15]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_76).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[16]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_77).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[17]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_78).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[18]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_79).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[19]/td[1]/text()').extract()
		elif "Manufacturer" in "".join(text_80).strip():
			manufacturer=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[20]/td[1]/text()').extract()
		

		#######################################################################################

		item_model_no=""
	  
		if "Item model number" in "".join(text_1).strip():
			item_model_no=sel.xpath('//*[@class="value"][1]//text()').extract()
		elif "Item model number" in "".join(text_2).strip():
			item_model_no=sel.xpath('//*[@class="value"][2]//text()').extract()
		elif "Item model number" in "".join(text_3).strip():
			item_model_no=sel.xpath('//*[@class="value"][3]//text()').extract()
		elif "Item model number" in "".join(text_4).strip():
			item_model_no=sel.xpath('//*[@class="value"][4]//text()').extract()
		elif "Item model number" in "".join(text_5).strip():
			item_model_no=sel.xpath('//*[@class="value"][5]//text()').extract()
		elif "Item model number" in "".join(text_6).strip():
			item_model_no=sel.xpath('//*[@class="value"][6]//text()').extract()
		elif "Item model number" in "".join(text_7).strip():
			item_model_no=sel.xpath('//*[@class="value"][7]//text()').extract()
		elif "Item model number" in "".join(text_8).strip():
			item_model_no=sel.xpath('//*[@class="value"][8]//text()').extract()
		elif "Item model number" in "".join(text_9).strip():
			item_model_no=sel.xpath('//*[@class="value"][9]//text()').extract()
		elif "Item model number" in "".join(text_10).strip():
			item_model_no=sel.xpath('//*[@class="value"][10]//text()').extract()
		elif "Item model number" in "".join(text_11).strip():
			item_model_no=sel.xpath('//*[@class="value"][11]//text()').extract()
		elif "Item model number" in "".join(text_12).strip():
			item_model_no=sel.xpath('//*[@class="value"][12]//text()').extract()
		elif "Item model number" in "".join(text_13).strip():
			item_model_no=sel.xpath('//*[@class="value"][13]//text()').extract()
		elif "Item model number" in "".join(text_14).strip():
			item_model_no=sel.xpath('//*[@class="value"][14]//text()').extract()
		elif "Item model number" in "".join(text_15).strip():
			item_model_no=sel.xpath('//*[@class="value"][15]//text()').extract()
		elif "Item model number" in "".join(text_16).strip():
			item_model_no=sel.xpath('//*[@class="value"][16]//text()').extract()
		elif "Item model number" in "".join(text_17).strip():
			item_model_no=sel.xpath('//*[@class="value"][17]//text()').extract()
		elif "Item model number" in "".join(text_18).strip():
			item_model_no=sel.xpath('//*[@class="value"][18]//text()').extract()
		elif "Item model number" in "".join(text_19).strip():
			item_model_no=sel.xpath('//*[@class="value"][19]//text()').extract()
		elif "Item model number" in "".join(text_20).strip():
			item_model_no=sel.xpath('//*[@class="value"][20]//text()').extract()
		
		elif "Item model number" in "".join(text_21).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_22).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_23).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_24).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_25).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_26).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_27).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_28).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_29).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_30).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_31).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_32).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_33).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_34).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_35).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_36).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_37).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_38).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_39).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_40).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/td[1]/text()').extract()
				

		elif "Item model number" in "".join(text_41).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/text()').extract()
		elif "Item model number" in "".join(text_42).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/text()').extract()
		elif "Item model number" in "".join(text_43).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/text()').extract()
		elif "Item model number" in "".join(text_44).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/text()').extract()
		elif "Item model number" in "".join(text_45).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/text()').extract()
		elif "Item model number" in "".join(text_46).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/text()').extract()
		elif "Item model number" in "".join(text_47).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/text()').extract()
		elif "Item model number" in "".join(text_48).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/text()').extract()
		elif "Item model number" in "".join(text_49).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/text()').extract()
		elif "Item model number" in "".join(text_50).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/text()').extract()
		elif "Item model number" in "".join(text_51).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/text()').extract()
		elif "Item model number" in "".join(text_52).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/text()').extract()
		elif "Item model number" in "".join(text_53).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/text()').extract()
		elif "Item model number" in "".join(text_54).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/text()').extract()
		elif "Item model number" in "".join(text_55).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/text()').extract()
		elif "Item model number" in "".join(text_56).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/text()').extract()
		elif "Item model number" in "".join(text_57).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/text()').extract()
		elif "Item model number" in "".join(text_58).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/text()').extract()
		elif "Item model number" in "".join(text_59).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/text()').extract()
		elif "Item model number" in "".join(text_60).strip():
			item_model_no=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/text()').extract()
		

		elif "Item model number" in "".join(text_61).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[1]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_62).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[2]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_63).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[3]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_64).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[4]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_65).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[5]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_66).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[6]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_67).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[7]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_68).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[8]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_69).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[9]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_70).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[10]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_71).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[11]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_72).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[12]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_73).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[13]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_74).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[14]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_75).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[15]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_76).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[16]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_77).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[17]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_78).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[18]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_79).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[19]/td[1]/text()').extract()
		elif "Item model number" in "".join(text_80).strip():
			item_model_no=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[20]/td[1]/text()').extract()
		
		#######################################################################################

		available_date=""
	  
		if "date first available" in "".join(text_1).strip().lower():
			available_date=sel.xpath('//*[@class="value"][1]//text()').extract()
		elif "date first available" in "".join(text_2).strip().lower():
			available_date=sel.xpath('//*[@class="value"][2]//text()').extract()
		elif "date first available" in "".join(text_3).strip().lower():
			available_date=sel.xpath('//*[@class="value"][3]//text()').extract()
		elif "date first available" in "".join(text_4).strip().lower():
			available_date=sel.xpath('//*[@class="value"][4]//text()').extract()
		elif "date first available" in "".join(text_5).strip().lower():
			available_date=sel.xpath('//*[@class="value"][5]//text()').extract()
		elif "date first available" in "".join(text_6).strip().lower():
			available_date=sel.xpath('//*[@class="value"][6]//text()').extract()
		elif "date first available" in "".join(text_7).strip().lower():
			available_date=sel.xpath('//*[@class="value"][7]//text()').extract()
		elif "date first available" in "".join(text_8).strip().lower():
			available_date=sel.xpath('//*[@class="value"][8]//text()').extract()
		elif "date first available" in "".join(text_9).strip().lower():
			available_date=sel.xpath('//*[@class="value"][9]//text()').extract()
		elif "date first available" in "".join(text_10).strip().lower():
			available_date=sel.xpath('//*[@class="value"][10]//text()').extract()
		elif "date first available" in "".join(text_11).strip().lower():
			available_date=sel.xpath('//*[@class="value"][11]//text()').extract()
		elif "date first available" in "".join(text_12).strip().lower():
			available_date=sel.xpath('//*[@class="value"][12]//text()').extract()
		elif "date first available" in "".join(text_13).strip().lower():
			available_date=sel.xpath('//*[@class="value"][13]//text()').extract()
		elif "date first available" in "".join(text_14).strip().lower():
			available_date=sel.xpath('//*[@class="value"][14]//text()').extract()
		elif "date first available" in "".join(text_15).strip().lower():
			available_date=sel.xpath('//*[@class="value"][15]//text()').extract()
		elif "date first available" in "".join(text_16).strip().lower():
			available_date=sel.xpath('//*[@class="value"][16]//text()').extract()
		elif "date first available" in "".join(text_17).strip().lower():
			available_date=sel.xpath('//*[@class="value"][17]//text()').extract()
		elif "date first available" in "".join(text_18).strip().lower():
			available_date=sel.xpath('//*[@class="value"][18]//text()').extract()
		elif "date first available" in "".join(text_19).strip().lower():
			available_date=sel.xpath('//*[@class="value"][19]//text()').extract()
		elif "date first available" in "".join(text_20).strip().lower():
			available_date=sel.xpath('//*[@class="value"][20]//text()').extract()
		
		elif "date first available" in "".join(text_21).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/text()').extract()
		elif "date first available" in "".join(text_22).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/td[1]/text()').extract()
		elif "date first available" in "".join(text_23).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/td[1]/text()').extract()
		elif "date first available" in "".join(text_24).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/td[1]/text()').extract()
		elif "date first available" in "".join(text_25).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/td[1]/text()').extract()
		elif "date first available" in "".join(text_26).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/td[1]/text()').extract()
		elif "date first available" in "".join(text_27).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/td[1]/text()').extract()
		elif "date first available" in "".join(text_28).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/td[1]/text()').extract()
		elif "date first available" in "".join(text_29).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/td[1]/text()').extract()
		elif "date first available" in "".join(text_30).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/td[1]/text()').extract()
		elif "date first available" in "".join(text_31).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/td[1]/text()').extract()
		elif "date first available" in "".join(text_32).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/td[1]/text()').extract()
		elif "date first available" in "".join(text_33).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/td[1]/text()').extract()
		elif "date first available" in "".join(text_34).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/td[1]/text()').extract()
		elif "date first available" in "".join(text_35).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/td[1]/text()').extract()
		elif "date first available" in "".join(text_36).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/td[1]/text()').extract()
		elif "date first available" in "".join(text_37).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/td[1]/text()').extract()
		elif "date first available" in "".join(text_38).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/td[1]/text()').extract()
		elif "date first available" in "".join(text_39).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/td[1]/text()').extract()
		elif "date first available" in "".join(text_40).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/td[1]/text()').extract()
		

		elif "date first available" in "".join(text_41).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/text()').extract()
		elif "date first available" in "".join(text_42).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/text()').extract()
		elif "date first available" in "".join(text_43).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/text()').extract()
		elif "date first available" in "".join(text_44).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/text()').extract()
		elif "date first available" in "".join(text_45).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/text()').extract()
		elif "date first available" in "".join(text_46).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/text()').extract()
		elif "date first available" in "".join(text_47).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/text()').extract()
		elif "date first available" in "".join(text_48).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/text()').extract()
		elif "date first available" in "".join(text_49).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/text()').extract()
		elif "date first available" in "".join(text_50).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/text()').extract()
		elif "date first available" in "".join(text_51).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/text()').extract()
		elif "date first available" in "".join(text_52).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/text()').extract()
		elif "date first available" in "".join(text_53).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/text()').extract()
		elif "date first available" in "".join(text_54).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/text()').extract()
		elif "date first available" in "".join(text_55).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/text()').extract()
		elif "date first available" in "".join(text_56).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/text()').extract()
		elif "date first available" in "".join(text_57).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/text()').extract()
		elif "date first available" in "".join(text_58).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/text()').extract()
		elif "date first available" in "".join(text_59).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/text()').extract()
		elif "date first available" in "".join(text_60).strip().lower():
			available_date=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/text()').extract()
		
		elif "date first available" in "".join(text_61).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[1]/td[1]/text()').extract()
		elif "date first available" in "".join(text_62).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[2]/td[1]/text()').extract()
		elif "date first available" in "".join(text_63).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[3]/td[1]/text()').extract()
		elif "date first available" in "".join(text_64).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[4]/td[1]/text()').extract()
		elif "date first available" in "".join(text_65).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[5]/td[1]/text()').extract()
		elif "date first available" in "".join(text_66).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[6]/td[1]/text()').extract()
		elif "date first available" in "".join(text_67).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[7]/td[1]/text()').extract()
		elif "date first available" in "".join(text_68).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[8]/td[1]/text()').extract()
		elif "date first available" in "".join(text_69).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[9]/td[1]/text()').extract()
		elif "date first available" in "".join(text_70).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[10]/td[1]/text()').extract()
		elif "date first available" in "".join(text_71).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[11]/td[1]/text()').extract()
		elif "date first available" in "".join(text_72).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[12]/td[1]/text()').extract()
		elif "date first available" in "".join(text_73).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[13]/td[1]/text()').extract()
		elif "date first available" in "".join(text_74).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[14]/td[1]/text()').extract()
		elif "date first available" in "".join(text_75).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[15]/td[1]/text()').extract()
		elif "date first available" in "".join(text_76).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[16]/td[1]/text()').extract()
		elif "date first available" in "".join(text_77).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[17]/td[1]/text()').extract()
		elif "date first available" in "".join(text_78).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[18]/td[1]/text()').extract()
		elif "date first available" in "".join(text_79).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[19]/td[1]/text()').extract()
		elif "date first available" in "".join(text_80).strip().lower():
			available_date=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[20]/td[1]/text()').extract()
		

		#######################################################################################

		color=""
	  
		if "Color" in "".join(text_1).strip():
			color=sel.xpath('//*[@class="value"][1]//text()').extract()
		elif "Color" in "".join(text_2).strip():
			color=sel.xpath('//*[@class="value"][2]//text()').extract()
		elif "Color" in "".join(text_3).strip():
			color=sel.xpath('//*[@class="value"][3]//text()').extract()
		elif "Color" in "".join(text_4).strip():
			color=sel.xpath('//*[@class="value"][4]//text()').extract()
		elif "Color" in "".join(text_5).strip():
			color=sel.xpath('//*[@class="value"][5]//text()').extract()
		elif "Color" in "".join(text_6).strip():
			color=sel.xpath('//*[@class="value"][6]//text()').extract()
		elif "Color" in "".join(text_7).strip():
			color=sel.xpath('//*[@class="value"][7]//text()').extract()
		elif "Color" in "".join(text_8).strip():
			color=sel.xpath('//*[@class="value"][8]//text()').extract()
		elif "Color" in "".join(text_9).strip():
			color=sel.xpath('//*[@class="value"][9]//text()').extract()
		elif "Color" in "".join(text_10).strip():
			color=sel.xpath('//*[@class="value"][10]//text()').extract()
		elif "Color" in "".join(text_11).strip():
			color=sel.xpath('//*[@class="value"][11]//text()').extract()
		elif "Color" in "".join(text_12).strip():
			color=sel.xpath('//*[@class="value"][12]//text()').extract()
		elif "Color" in "".join(text_13).strip():
			color=sel.xpath('//*[@class="value"][13]//text()').extract()
		elif "Color" in "".join(text_14).strip():
			color=sel.xpath('//*[@class="value"][14]//text()').extract()
		elif "Color" in "".join(text_15).strip():
			color=sel.xpath('//*[@class="value"][15]//text()').extract()
		elif "Color" in "".join(text_16).strip():
			color=sel.xpath('//*[@class="value"][16]//text()').extract()
		elif "Color" in "".join(text_17).strip():
			color=sel.xpath('//*[@class="value"][17]//text()').extract()
		elif "Color" in "".join(text_18).strip():
			color=sel.xpath('//*[@class="value"][18]//text()').extract()
		elif "Color" in "".join(text_19).strip():
			color=sel.xpath('//*[@class="value"][19]//text()').extract()
		elif "Color" in "".join(text_20).strip():
			color=sel.xpath('//*[@class="value"][20]//text()').extract()
		
		elif "Color" in "".join(text_21).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/text()').extract()
		elif "Color" in "".join(text_22).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/td[1]/text()').extract()
		elif "Color" in "".join(text_23).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/td[1]/text()').extract()
		elif "Color" in "".join(text_24).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/td[1]/text()').extract()
		elif "Color" in "".join(text_25).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/td[1]/text()').extract()
		elif "Color" in "".join(text_26).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/td[1]/text()').extract()
		elif "Color" in "".join(text_27).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/td[1]/text()').extract()
		elif "Color" in "".join(text_28).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/td[1]/text()').extract()
		elif "Color" in "".join(text_29).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/td[1]/text()').extract()
		elif "Color" in "".join(text_30).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/td[1]/text()').extract()
		elif "Color" in "".join(text_31).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/td[1]/text()').extract()
		elif "Color" in "".join(text_32).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/td[1]/text()').extract()
		elif "Color" in "".join(text_33).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/td[1]/text()').extract()
		elif "Color" in "".join(text_34).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/td[1]/text()').extract()
		elif "Color" in "".join(text_35).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/td[1]/text()').extract()
		elif "Color" in "".join(text_36).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/td[1]/text()').extract()
		elif "Color" in "".join(text_37).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/td[1]/text()').extract()
		elif "Color" in "".join(text_38).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/td[1]/text()').extract()
		elif "Color" in "".join(text_39).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/td[1]/text()').extract()
		elif "Color" in "".join(text_40).strip():
			color=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/td[1]/text()').extract()
		

		elif "Color" in "".join(text_41).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/text()').extract()
		elif "Color" in "".join(text_42).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/text()').extract()
		elif "Color" in "".join(text_43).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/text()').extract()
		elif "Color" in "".join(text_44).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/text()').extract()
		elif "Color" in "".join(text_45).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/text()').extract()
		elif "Color" in "".join(text_46).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/text()').extract()
		elif "Color" in "".join(text_47).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/text()').extract()
		elif "Color" in "".join(text_48).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/text()').extract()
		elif "Color" in "".join(text_49).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/text()').extract()
		elif "Color" in "".join(text_50).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/text()').extract()
		elif "Color" in "".join(text_51).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/text()').extract()
		elif "Color" in "".join(text_52).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/text()').extract()
		elif "Color" in "".join(text_53).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/text()').extract()
		elif "Color" in "".join(text_54).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/text()').extract()
		elif "Color" in "".join(text_55).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/text()').extract()
		elif "Color" in "".join(text_56).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/text()').extract()
		elif "Color" in "".join(text_57).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/text()').extract()
		elif "Color" in "".join(text_58).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/text()').extract()
		elif "Color" in "".join(text_59).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/text()').extract()
		elif "Color" in "".join(text_60).strip():
			color=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/text()').extract()
		

		elif "Color" in "".join(text_61).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[1]/td[1]/text()').extract()
		elif "Color" in "".join(text_62).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[2]/td[1]/text()').extract()
		elif "Color" in "".join(text_63).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[3]/td[1]/text()').extract()
		elif "Color" in "".join(text_64).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[4]/td[1]/text()').extract()
		elif "Color" in "".join(text_65).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[5]/td[1]/text()').extract()
		elif "Color" in "".join(text_66).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[6]/td[1]/text()').extract()
		elif "Color" in "".join(text_67).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[7]/td[1]/text()').extract()
		elif "Color" in "".join(text_68).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[8]/td[1]/text()').extract()
		elif "Color" in "".join(text_69).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[9]/td[1]/text()').extract()
		elif "Color" in "".join(text_70).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[10]/td[1]/text()').extract()
		elif "Color" in "".join(text_71).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[11]/td[1]/text()').extract()
		elif "Color" in "".join(text_72).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[12]/td[1]/text()').extract()
		elif "Color" in "".join(text_73).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[13]/td[1]/text()').extract()
		elif "Color" in "".join(text_74).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[14]/td[1]/text()').extract()
		elif "Color" in "".join(text_75).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[15]/td[1]/text()').extract()
		elif "Color" in "".join(text_76).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[16]/td[1]/text()').extract()
		elif "Color" in "".join(text_77).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[17]/td[1]/text()').extract()
		elif "Color" in "".join(text_78).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[18]/td[1]/text()').extract()
		elif "Color" in "".join(text_79).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[19]/td[1]/text()').extract()
		elif "Color" in "".join(text_80).strip():
			color=sel.xpath('//*[@id="productDetails_techSpec_section_2"]/tr[20]/td[1]/text()').extract()
		
		#asin=sel.xpath('//*[@id="averageCustomerReviews"][1]/@data-asin').extract()
		rank=sel.xpath('//*[@id="SalesRank"]//text()').extract()


		if len("".join(rank))>0:
			try:
				rank="".join(rank).strip().split('Best Sellers Rank:')[1]
			except:
				pass

		if len("".join(rank))<=0:
			if "Best Sellers Rank" in "".join(text_1).strip():
				rank=sel.xpath('//*[@class="value"][1]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_2).strip():
				rank=sel.xpath('//*[@class="value"][2]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_3).strip():
				rank=sel.xpath('//*[@class="value"][3]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_4).strip():
				rank=sel.xpath('//*[@class="value"][4]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_5).strip():
				rank=sel.xpath('//*[@class="value"][5]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_6).strip():
				rank=sel.xpath('//*[@class="value"][6]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_7).strip():
				rank=sel.xpath('//*[@class="value"][7]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_8).strip():
				rank=sel.xpath('//*[@class="value"][8]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_9).strip():
				rank=sel.xpath('//*[@class="value"][9]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_10).strip():
				rank=sel.xpath('//*[@class="value"][10]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_11).strip():
				rank=sel.xpath('//*[@class="value"][11]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_12).strip():
				rank=sel.xpath('//*[@class="value"][12]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_13).strip():
				rank=sel.xpath('//*[@class="value"][13]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_14).strip():
				rank=sel.xpath('//*[@class="value"][14]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_15).strip():
				rank=sel.xpath('//*[@class="value"][15]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_16).strip():
				rank=sel.xpath('//*[@class="value"][16]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_17).strip():
				rank=sel.xpath('//*[@class="value"][17]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_18).strip():
				rank=sel.xpath('//*[@class="value"][18]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_19).strip():
				rank=sel.xpath('//*[@class="value"][19]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_20).strip():
				rank=sel.xpath('//*[@class="value"][20]//text()').extract()
			
			elif "Best Sellers Rank" in "".join(text_21).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[1]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_22).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[2]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_23).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[3]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_24).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[4]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_25).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[5]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_26).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[6]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_27).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[7]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_28).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[8]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_29).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[9]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_30).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[10]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_31).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[11]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_32).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[12]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_33).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[13]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_34).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[14]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_35).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[15]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_36).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[16]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_37).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[17]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_38).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[18]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_39).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[19]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_40).strip():
				rank=sel.xpath('//*[@id="productDetails_detailBullets_sections1"]/tr[20]/td[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			

			elif "Best Sellers Rank" in "".join(text_41).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[1]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_42).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[2]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_43).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[3]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_44).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[4]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_45).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[5]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_46).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[6]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_47).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[7]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_48).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[8]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_49).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[9]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_50).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[10]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_51).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[11]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_52).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[12]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_53).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[13]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_54).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[14]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_55).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[15]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_56).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[16]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_57).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[17]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_58).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[18]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_59).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[19]/span/span[position()>=1 and not(position>=100)]//text()').extract()
			elif "Best Sellers Rank" in "".join(text_60).strip():
				rank=sel.xpath('//*[@id="detail-bullets"]/table/tr/td/div/ul/li[20]/span/span[position()>=1 and not(position>=100)]//text()').extract()
		
		rank_1=""
		rank_2=""
		rank_3=""
		rank_4=""
		rank_list=[]
		try:
			temp_rank="".join(rank).split('#')
			for t in temp_rank:
				if len("".join(t).strip().replace(' ','').strip())>0:
					rank_list.append("".join(t).strip())

		except:
			pass

		if len(rank_list)>0:
			rank_1=rank_list[0].split(' in')[0].strip()+" in "+rank_list[0].split(' in')[1].strip()
		if len(rank_list)>1:
			rank_2=rank_list[1].split(' in')[0].strip()+" in "+rank_list[1].split(' in')[1].strip()
		if len(rank_list)>2:
			rank_3=rank_list[2].split(' in')[0].strip()+" in "+rank_list[2].split(' in')[1].strip()
		if len(rank_list)>3:
			rank_4=rank_list[3].split(' in')[0].strip()+" in "+rank_list[3].split(' in')[1].strip()
		
		brand=sel.xpath('//*[@id="brand"][1]//text()').extract()
		brand_url=sel.xpath('//*[@id="brand"][1]/@href').extract()
		try:
			brand_url="http://www.amazon.com"+brand_url[0]
		except:
			pass

		title=sel.xpath('//*[@id="productTitle"]//text()').extract()
		rating=sel.xpath('//*[@class="reviewCountTextLinkedHistogram noUnderline"]/@title').extract()
		try:
			rating="".join(rating).split()[0]
		except:
			pass

		reviews=sel.xpath('//*[@id="acrCustomerReviewText"]/text()').extract()
		try:
			reviews="".join(reviews).split()[0]
		except:
			pass


		style_text=sel.xpath('//*[@id="variation_style_name"][1]/div/label/text()').extract()
		style_name=""
		style=""
		size=sel.xpath('//*[@id="variation_size_name"][1]/div/span[@class="selection"]/text()').extract()
		
		if len("".join(size).strip())<=0:
			size=sel.xpath('//*[@id="vodd-button-label-size_name"]/text()').extract()

		if "Style Name:" in "".join(style_text).strip().strip():
			style_name=sel.xpath('//*[@id="variation_style_name"][1]/div/span[@class="selection"]/text()').extract()
		
		if "Style:" in "".join(style_text).strip().strip():
			style=sel.xpath('//*[@id="variation_style_name"][1]/div/span[@class="selection"]/text()').extract()
		
		if len("".join(color).strip())<=0:
			color=sel.xpath('//*[@id="variation_color_name"][1]/div/span[@class="selection"]/text()').extract()
		
		if len("".join(color).strip())<=0:
			color=sel.xpath('//*[@id="vodd-button-label-color_name"]/text()').extract()

		color_size_text_1=sel.xpath('//*[@class="disclaim"][1]/text()[1]').extract()
		color_size_text_2=sel.xpath('//*[@class="disclaim"][1]/text()[2]').extract()
		color_size_text_3=sel.xpath('//*[@class="disclaim"][1]/text()[3]').extract()
		color_size_text_4=sel.xpath('//*[@class="disclaim"][1]/text()[4]').extract()
		color_size_text_5=sel.xpath('//*[@class="disclaim"][1]/text()[5]').extract()
		color_size_text_6=sel.xpath('//*[@class="disclaim"][2]/text()[1]').extract()
		color_size_text_7=sel.xpath('//*[@class="disclaim"][2]/text()[2]').extract()
		color_size_text_8=sel.xpath('//*[@class="disclaim"][2]/text()[3]').extract()
		color_size_text_9=sel.xpath('//*[@class="disclaim"][2]/text()[4]').extract()
		color_size_text_10=sel.xpath('//*[@class="disclaim"][2]/text()[5]').extract()
		color_size_text_11=sel.xpath('//*[@class="disclaim"][3]/text()[1]').extract()
		color_size_text_12=sel.xpath('//*[@class="disclaim"][3]/text()[2]').extract()
		color_size_text_13=sel.xpath('//*[@class="disclaim"][3]/text()[3]').extract()
		color_size_text_14=sel.xpath('//*[@class="disclaim"][3]/text()[4]').extract()
		color_size_text_15=sel.xpath('//*[@class="disclaim"][3]/text()[5]').extract()

		if len("".join(color))<=0:
			if "Color" in "".join(color_size_text_1).strip():
				color=sel.xpath('//*[@class="disclaim"][1]/strong[1]/text()').extract()
			elif "Color" in "".join(color_size_text_2).strip():
				color=sel.xpath('//*[@class="disclaim"][1]/strong[2]/text()').extract()
			elif "Color" in "".join(color_size_text_3).strip():
				color=sel.xpath('//*[@class="disclaim"][1]/strong[3]/text()').extract()
			elif "Color" in "".join(color_size_text_4).strip():
				color=sel.xpath('//*[@class="disclaim"][1]/strong[4]/text()').extract()
			elif "Color" in "".join(color_size_text_5).strip():
				color=sel.xpath('//*[@class="disclaim"][1]/strong[5]/text()').extract()
			elif "Color" in "".join(color_size_text_6).strip():
				color=sel.xpath('//*[@class="disclaim"][2]/strong[1]/text()').extract()
			elif "Color" in "".join(color_size_text_7).strip():
				color=sel.xpath('//*[@class="disclaim"][2]/strong[2]/text()').extract()
			elif "Color" in "".join(color_size_text_8).strip():
				color=sel.xpath('//*[@class="disclaim"][2]/strong[3]/text()').extract()
			elif "Color" in "".join(color_size_text_9).strip():
				color=sel.xpath('//*[@class="disclaim"][2]/strong[4]/text()').extract()
			elif "Color" in "".join(color_size_text_10).strip():
				color=sel.xpath('//*[@class="disclaim"][2]/strong[5]/text()').extract()
			elif "Color" in "".join(color_size_text_11).strip():
				color=sel.xpath('//*[@class="disclaim"][3]/strong[1]/text()').extract()
			elif "Color" in "".join(color_size_text_12).strip():
				color=sel.xpath('//*[@class="disclaim"][3]/strong[2]/text()').extract()
			elif "Color" in "".join(color_size_text_13).strip():
				color=sel.xpath('//*[@class="disclaim"][3]/strong[3]/text()').extract()
			elif "Color" in "".join(color_size_text_14).strip():
				color=sel.xpath('//*[@class="disclaim"][3]/strong[4]/text()').extract()
			elif "Color" in "".join(color_size_text_15).strip():
				color=sel.xpath('//*[@class="disclaim"][3]/strong[5]/text()').extract()


		if len("".join(size))<=0:
			if "Size" in "".join(color_size_text_1).strip():
				size=sel.xpath('//*[@class="disclaim"][1]/strong[1]/text()').extract()
			elif "Size" in "".join(color_size_text_2).strip():
				size=sel.xpath('//*[@class="disclaim"][1]/strong[2]/text()').extract()
			elif "Size" in "".join(color_size_text_3).strip():
				size=sel.xpath('//*[@class="disclaim"][1]/strong[3]/text()').extract()
			elif "Size" in "".join(color_size_text_4).strip():
				size=sel.xpath('//*[@class="disclaim"][1]/strong[4]/text()').extract()
			elif "Size" in "".join(color_size_text_5).strip():
				size=sel.xpath('//*[@class="disclaim"][1]/strong[5]/text()').extract()
			elif "Size" in "".join(color_size_text_6).strip():
				size=sel.xpath('//*[@class="disclaim"][2]/strong[1]/text()').extract()
			elif "Size" in "".join(color_size_text_7).strip():
				size=sel.xpath('//*[@class="disclaim"][2]/strong[2]/text()').extract()
			elif "Size" in "".join(color_size_text_8).strip():
				size=sel.xpath('//*[@class="disclaim"][2]/strong[3]/text()').extract()
			elif "Size" in "".join(color_size_text_9).strip():
				size=sel.xpath('//*[@class="disclaim"][2]/strong[4]/text()').extract()
			elif "Size" in "".join(color_size_text_10).strip():
				size=sel.xpath('//*[@class="disclaim"][2]/strong[5]/text()').extract()
			elif "Size" in "".join(color_size_text_11).strip():
				size=sel.xpath('//*[@class="disclaim"][3]/strong[1]/text()').extract()
			elif "Size" in "".join(color_size_text_12).strip():
				size=sel.xpath('//*[@class="disclaim"][3]/strong[2]/text()').extract()
			elif "Size" in "".join(color_size_text_13).strip():
				size=sel.xpath('//*[@class="disclaim"][3]/strong[3]/text()').extract()
			elif "Size" in "".join(color_size_text_14).strip():
				size=sel.xpath('//*[@class="disclaim"][3]/strong[4]/text()').extract()
			elif "Size" in "".join(color_size_text_15).strip():
				size=sel.xpath('//*[@class="disclaim"][3]/strong[5]/text()').extract()


		if len("".join(style))<=0:
			if "Style:" == "".join(color_size_text_1).strip():
				style=sel.xpath('//*[@class="disclaim"][1]/strong[1]/text()').extract()
			elif "Style:" == "".join(color_size_text_2).strip():
				style=sel.xpath('//*[@class="disclaim"][1]/strong[2]/text()').extract()
			elif "Style:" == "".join(color_size_text_3).strip():
				style=sel.xpath('//*[@class="disclaim"][1]/strong[3]/text()').extract()
			elif "Style:" == "".join(color_size_text_4).strip():
				style=sel.xpath('//*[@class="disclaim"][1]/strong[4]/text()').extract()
			elif "Style:" == "".join(color_size_text_5).strip():
				style=sel.xpath('//*[@class="disclaim"][1]/strong[5]/text()').extract()
			elif "Style:" == "".join(color_size_text_6).strip():
				style=sel.xpath('//*[@class="disclaim"][2]/strong[1]/text()').extract()
			elif "Style:" == "".join(color_size_text_7).strip():
				style=sel.xpath('//*[@class="disclaim"][2]/strong[2]/text()').extract()
			elif "Style:" == "".join(color_size_text_8).strip():
				style=sel.xpath('//*[@class="disclaim"][2]/strong[3]/text()').extract()
			elif "Style:" == "".join(color_size_text_9).strip():
				style=sel.xpath('//*[@class="disclaim"][2]/strong[4]/text()').extract()
			elif "Style:" == "".join(color_size_text_10).strip():
				style=sel.xpath('//*[@class="disclaim"][2]/strong[5]/text()').extract()
			elif "Style:" == "".join(color_size_text_11).strip():
				style=sel.xpath('//*[@class="disclaim"][3]/strong[1]/text()').extract()
			elif "Style:" == "".join(color_size_text_12).strip():
				style=sel.xpath('//*[@class="disclaim"][3]/strong[2]/text()').extract()
			elif "Style:" == "".join(color_size_text_13).strip():
				style=sel.xpath('//*[@class="disclaim"][3]/strong[3]/text()').extract()
			elif "Style:" == "".join(color_size_text_14).strip():
				style=sel.xpath('//*[@class="disclaim"][3]/strong[4]/text()').extract()
			elif "Style:" == "".join(color_size_text_15).strip():
				style=sel.xpath('//*[@class="disclaim"][3]/strong[5]/text()').extract()


		if len("".join(style_name))<=0:
			if "Style Name:" in "".join(color_size_text_1).strip():
				style_name=sel.xpath('//*[@class="disclaim"][1]/strong[1]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_2).strip():
				style_name=sel.xpath('//*[@class="disclaim"][1]/strong[2]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_3).strip():
				style_name=sel.xpath('//*[@class="disclaim"][1]/strong[3]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_4).strip():
				style_name=sel.xpath('//*[@class="disclaim"][1]/strong[4]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_5).strip():
				style_name=sel.xpath('//*[@class="disclaim"][1]/strong[5]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_6).strip():
				style_name=sel.xpath('//*[@class="disclaim"][2]/strong[1]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_7).strip():
				style_name=sel.xpath('//*[@class="disclaim"][2]/strong[2]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_8).strip():
				style_name=sel.xpath('//*[@class="disclaim"][2]/strong[3]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_9).strip():
				style_name=sel.xpath('//*[@class="disclaim"][2]/strong[4]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_10).strip():
				style_name=sel.xpath('//*[@class="disclaim"][2]/strong[5]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_11).strip():
				style_name=sel.xpath('//*[@class="disclaim"][3]/strong[1]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_12).strip():
				style_name=sel.xpath('//*[@class="disclaim"][3]/strong[2]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_13).strip():
				style_name=sel.xpath('//*[@class="disclaim"][3]/strong[3]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_14).strip():
				style_name=sel.xpath('//*[@class="disclaim"][3]/strong[4]/text()').extract()
			elif "Style Name:" in "".join(color_size_text_15).strip():
				style_name=sel.xpath('//*[@class="disclaim"][3]/strong[5]/text()').extract()


		price=sel.xpath('//*[@id="priceblock_ourprice"]/text()').extract()
		sale_price=sel.xpath('//*[@id="priceblock_saleprice"][1]/text()').extract()
		other_shipping=sel.xpath('//*[@id="ourprice_shippingmessage"]/span//text()').extract()
		sale_price_shipping_message=sel.xpath('//*[@id="saleprice_shippingmessage"]/span//text()').extract()
		quantity=sel.xpath('//*[@id="availability"]//text()').extract()
		shipper=sel.xpath('//*[@id="merchant-info"]//text()').extract()
		
		merchantId=sel.xpath('//*[@id="merchantID"]/@value').extract()

		url="http://www.amazon.com/gp/product/features/dp-fast-track/udp-ajax-handler/get-quantity-update-message.html?ie=UTF8&asin="+"".join(asin).strip()+"&quantity="+"".join(quantity).strip()+"&merchantId="+"".join(merchantId).strip()
		
		#//div[contains(@id,"feature-bullets")]
		features=sel.xpath('//*[@id="feature-bullets"]').extract()#/ul/li[position()>=1 and not (position()>=100)]/span/text()').extract()
		if len("".join(features))<=0:
			features=sel.xpath('//*[@class="bucket normal"]/div').extract()#/ul/li[position()>=1 and not (position()>=100)]/text()').extract()
		
		
		description=sel.xpath('//*[@class="productDescriptionWrapper"]').extract()
		#features="Features:"+"".join(features).strip()+"--"+"Description:"+"".join(description).strip()
		features="<p><strong><span style=\"font-size:14px;\"><span style=\"font-family:arial,helvetica,sans-serif;\">Description:</span></span></strong></p>"+ "<p>"+"".join(description).strip()+"</p>"+"<br></br>"+"<p><strong><span style=\"font-family: arial, helvetica, sans-serif; font-size: 14px; line-height: 1.6;\">Features:</span></strong></p>"+"".join(features).strip()
		
		# all_html=response.body
		# images=all_html.split("\"large\":\"")
		# if total_images>1:
		#	 image_1=images[1].split('\"')[0]
		# if total_images>2:
		#	 image_2=images[2].split('\"')[0]
		# if total_images>3:
		#	 image_3=images[3].split('\"')[0]
		# if total_images>4:
		#	 image_4=images[4].split('\"')[0]
		# if total_images>5:
		#	 image_5=images[5].split('\"')[0]
		# if total_images>6:
		#	 image_6=images[6].split('\"')[0]
		# if total_images>7:
		#	 image_7=images[7].split('\"')[0]
	  

		images_temp=sel.xpath('//*[@class="a-button-text"]/img/@src').extract()
		images_temp_1=sel.xpath('//*[@id="imgTagWrapperId"]/img/@src').extract()
		images=images_temp+images_temp_1
		total_images=len(images)

		image_1=""
		image_2=""
		image_3=""
		image_4=""
		image_5=""
		image_6=""
		image_7=""
		
		if total_images>1:
			image_1=images[1].split('_')[0]+"jpg"
		if total_images>2:
			image_2=images[2].split('_')[0]+"jpg"
		if total_images>3:
			image_3=images[3].split('_')[0]+"jpg"
		if total_images>4:
			image_4=images[4].split('_')[0]+"jpg"
		if total_images>5:
			image_5=images[5].split('_')[0]+"jpg"
		if total_images>6:
			image_6=images[6].split('_')[0]+"jpg"
		if total_images>7:
			image_7=images[7].split('_')[0]+"jpg"
	  
		shipping_time=sel.xpath('//*[@id="fast-track-message"]/div//text()').extract()
		store_name=sel.xpath('//*[@id="merchant-info"]/a[1]/text()').extract()
		if len("".join(store_name))<=0:
			store_name=sel.xpath('//*[@id="merchant-info"]/text()').extract()
			if "by" in "".join(store_name).strip():
				try:
					store_name="".join(store_name).split('by')[1].strip().split()[0]
				except:
					store_name="".join(store_name).split('by')[1].strip()

		add_on=sel.xpath('//*[@class="a-box a-first a-text-center addOnItem-header"]/div/span/text()').extract()
		
		if len("".join(add_on))<=0:
			add_on="No"

		# item=AmazonStoreItem()
		item={}
		# item['category']=response.meta['cat']
		item['category']=category
		item['asin']="".join(asin).strip()
		item['store_name']="".join(store_name).strip()
		item['add_on']="".join(add_on).strip()
		# item['product_url']=response.url
		item['product_url']=page_url
		item['title']="".join(title).strip()
		item['rating']=rating
		item['reviews']=reviews
		if len("".join(price))>0:
			item['price']="".join(price).encode('ascii','ignore').strip()
		else:
			item['price']="".join(sale_price).encode('ascii','ignore').strip()
		if len("".join(other_shipping))>0:
			item['other_shipping']="".join(other_shipping).strip()
		else:
			item['other_shipping']="".join(sale_price_shipping_message).strip()

		item['quantity']="".join(quantity).strip()
		item['shipper']="".join(shipper).strip()
		item['shipping_time']="".join(shipping_time).strip()
		item['features']=features
		item['image_1']=image_1
		item['image_2']=image_2
		item['image_3']=image_3
		item['image_4']=image_4
		item['image_5']=image_5
		item['image_6']=image_6
		item['image_7']=image_7
		item['brand']="".join(brand).strip()
		item['brand_url']=brand_url
		item['UPC']="".join(upc).strip()
		item['Manufacturer']="".join(manufacturer).strip()
		item['ItemModelNo']="".join(item_model_no).strip()
		item['AvailableDate']="".join(available_date).strip()
		item['Color']="".join(color).strip()
		item['Size']="".join(size).strip()
		item['Style']="".join(style).strip()
		item['StyleName']="".join(style_name).strip()
		item['Rank1']="".join(rank_1).strip()
		item['Rank2']="".join(rank_2).strip()
		item['Rank3']="".join(rank_3).strip()
		item['Rank4']="".join(rank_4).strip()
		# yield item
		return item





if __name__ == "__main__":

	# scrapy = AmazonComCrawler(["https://www.amazon.com/s/ref=nb_sb_noss/147-5815053-6611565?url=search-alias%3Dfashion-baby&field-keywords=replacement+parts&rh=n%3A7147444011%2Ck%3Areplacement+parts"])
	scrapy = AmazonComCrawler(["https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Damazon-devices&field-keywords=replacement+parts&rh=n%3A16333372011%2Ck%3Areplacement+parts"])
	# scrapy = AmazonComCrawler(["https://www.amazon.com/s/ref=nb_sb_noss/131-2468049-1201558?url=search-alias%3Dappliances&field-keywords=replacement+parts&rh=n%3A2619525011%2Ck%3Areplacement+parts"])
	# scrapy = AmazonComCrawler(["https://www.amazon.com/s/ref=nb_sb_noss/146-3364304-7220626?url=search-alias%3Dpopular&field-keywords=replacement+parts&rh=n%3A5174%2Ck%3Areplacement+parts"])
