# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import Item, Field


class AmazonStoreItem(scrapy.Item):
	category=scrapy.Field()
	asin=scrapy.Field()
	store_name=scrapy.Field()
	add_on=scrapy.Field()
	product_url=scrapy.Field()
	title=scrapy.Field()
	rating=scrapy.Field()
	reviews=scrapy.Field()
	price=scrapy.Field()
	other_shipping=scrapy.Field()
	quantity=scrapy.Field()
	shipper=scrapy.Field()
	shipping_time=scrapy.Field()
	features=scrapy.Field()
	brand=scrapy.Field()
	brand_url=scrapy.Field()
	UPC=scrapy.Field()
	Manufacturer=scrapy.Field()
	ItemModelNo=scrapy.Field()
	AvailableDate=scrapy.Field()
	Color=scrapy.Field()
	Size=scrapy.Field()
	Style=scrapy.Field()
	StyleName=scrapy.Field()
	Rank1=scrapy.Field()
	Rank2=scrapy.Field()
	Rank3=scrapy.Field()
	Rank4=scrapy.Field()
	image_1=scrapy.Field()
	image_2=scrapy.Field()
	image_3=scrapy.Field()
	image_4=scrapy.Field()
	image_5=scrapy.Field()
	image_6=scrapy.Field()
	image_7=scrapy.Field()
	
	
